package modules

import (
	"gitlab.com/antoxa2614/go-rpc-auth/internal/infrastructure/component"
	uservice "gitlab.com/antoxa2614/go-rpc-auth/internal/infrastructure/service/user"
	aservice "gitlab.com/antoxa2614/go-rpc-auth/internal/modules/auth/service"

	"gitlab.com/antoxa2614/go-rpc-auth/internal/storages"
)

type Services struct {
	User          uservice.Userer
	Auth          aservice.Auther
	UserClientRPC uservice.Userer
}

func NewServices(storages *storages.Storages, userService uservice.Userer, components *component.Components) *Services {
	//userService := uservice.NewUserService(storages.User, components.Logger)
	return &Services{
		User: userService,
		Auth: aservice.NewAuth(userService, storages.Verify, components),
	}
}
