package modules

import (
	"gitlab.com/antoxa2614/go-rpc-auth/internal/infrastructure/component"
	acontroller "gitlab.com/antoxa2614/go-rpc-auth/internal/modules/auth/controller"
)

type Controllers struct {
	Auth acontroller.Auther
	//User ucontroller.Userer
}

func NewControllers(services *Services, components *component.Components) *Controllers {
	authController := acontroller.NewAuth(services.Auth, components)
	//userController := ucontroller.NewUser(services.User, components)

	return &Controllers{
		Auth: authController,
		//User: userController,
	}
}
