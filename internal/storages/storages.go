package storages

import (
	"gitlab.com/antoxa2614/go-rpc-auth/internal/db/adapter"
	"gitlab.com/antoxa2614/go-rpc-auth/internal/infrastructure/cache"
	vstorage "gitlab.com/antoxa2614/go-rpc-auth/internal/modules/auth/storage"
)

type Storages struct {
	//User   ustorage.Userer
	Verify vstorage.Verifier
}

func NewStorages(sqlAdapter *adapter.SQLAdapter, cache cache.Cache) *Storages {
	return &Storages{
		//User:   ustorage.NewUserStorage(sqlAdapter, cache),
		Verify: vstorage.NewEmailVerify(sqlAdapter),
	}
}
