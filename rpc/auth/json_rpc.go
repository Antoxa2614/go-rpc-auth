package auth

import (
	"context"
	"log"

	"gitlab.com/antoxa2614/go-rpc-auth/internal/modules/auth/service"
)

// AuthServiceJSONRPC представляет AuthService для использования в JSON-RPC
type AuthServiceJSONRPC struct {
	authService service.Auther
}

// NewAuthServiceJSONRPC возвращает новый AuthServiceJSONRPC
func NewAuthServiceJSONRPC(authService service.Auther) *AuthServiceJSONRPC {
	return &AuthServiceJSONRPC{authService: authService}
}

// RegisterUser обрабатывает JSON-RPC запрос на регистрацию пользователя.
// Принимает входные данные, использует контекст по умолчанию и вызывает метод RegisterUser из authService.
// Возвращает выходные данные.
func (t *AuthServiceJSONRPC) RegisterUser(in service.RegisterIn, out *service.RegisterOut) error {
	log.Printf("start auth register user rpc method")
	*out = t.authService.Register(context.Background(), in)
	return nil
}

// AuthorizeEmail обрабатывает JSON-RPC запрос на авторизацию по e-mail пользователя.
// Принимает входные данные, использует контекст по умолчанию и вызывает метод AuthorizeEmail из authService.
// Возвращает выходные данные.
func (t *AuthServiceJSONRPC) AuthorizeEmail(in service.AuthorizeEmailIn, out *service.AuthorizeOut) error {
	*out = t.authService.AuthorizeEmail(context.Background(), in)
	return nil
}

// AuthorizeRefresh обрабатывает JSON-RPC запрос на обновление ключей авторизации пользователя.
// Принимает входные данные, использует контекст по умолчанию и вызывает метод AuthorizeRefresh из authService.
// Возвращает выходные данные.
func (t *AuthServiceJSONRPC) AuthorizeRefresh(in service.AuthorizeRefreshIn, out *service.AuthorizeOut) error {
	*out = t.authService.AuthorizeRefresh(context.Background(), in)
	return nil
}

// AuthorizePhone обрабатывает JSON-RPC запрос на авторизацию по телефону пользователя.
// Принимает входные данные, использует контекст по умолчанию и вызывает метод AuthorizePhone из authService.
// Возвращает выходные данные.
func (t *AuthServiceJSONRPC) AuthorizePhone(in service.AuthorizePhoneIn, out *service.AuthorizeOut) error {
	*out = t.authService.AuthorizePhone(context.Background(), in)
	return nil
}

// SendPhoneCode обрабатывает JSON-RPC запрос на отправку кода подтверждения на телефон пользователя.
// Принимает входные данные, использует контекст по умолчанию и вызывает метод SendPhoneCode из authService.
// Возвращает выходные данные.
func (t *AuthServiceJSONRPC) SendPhoneCode(in service.SendPhoneCodeIn, out *service.SendPhoneCodeOut) error {
	*out = t.authService.SendPhoneCode(context.Background(), in)
	return nil
}

// VerifyEmail обрабатывает JSON-RPC запрос на подтверждение email пользователя.
// Принимает входные данные, использует контекст по умолчанию и вызывает метод VerifyEmail из authService.
// Возвращает выходные данные.
func (t *AuthServiceJSONRPC) VerifyEmail(in service.VerifyEmailIn, out *service.VerifyEmailOut) error {
	*out = t.authService.VerifyEmail(context.Background(), in)
	return nil
}
